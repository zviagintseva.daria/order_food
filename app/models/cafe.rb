class Cafe < ApplicationRecord
	has_one_attached :image
	# validates :image,  file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }

	has_many :dishes
end
