ActiveAdmin.register Cafe do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :title, :desc, :image
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


# permit_params :title, :image, :desc

form do |f|
    f.inputs do
      f.input :title
      f.input :desc
      if f.object.image.attached?
        f.input :image,
          :as => :file,
          :hint => image_tag(
            url_for(
              f.object.image.variant(
                combine_options: {
                  gravity: 'Center',
                  crop: '50x50+0+0'
                }
              )
            )
          )
        else
          f.input :image, :as => :file
      end
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :image do |cafe|
      image_tag cafe.image.variant(combine_options: { gravity: 'Center', crop: '80x80+0+0' })
    end
    column :title do |cafe|
      link_to cafe.title, admin_cafe_path(cafe)
    end
    column :desc
    actions
  end

  show do
    attributes_table do
      row :image do |cafe|
        image_tag cafe.image.variant(combine_options: { gravity: 'Center', crop: '200x200+0' })
      end
      row :title
      row :desc
    end
    admin_cafes_path
    end
end
