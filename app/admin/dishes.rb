ActiveAdmin.register Dish do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
permit_params :title, :price, :desc, :image, :cafe_id

form do |f|
    f.inputs do
      f.input :title
      f.input :price
      f.input :desc
      if f.object.image.attached?
        f.input :image,
          :as => :file,
          :hint => image_tag(
            url_for(
              f.object.image.variant(
                combine_options: {
                  gravity: 'Center',
                  crop: '50x50+0+0'
                }
              )
            )
          )
        else
          f.input :image, :as => :file
      end
      f.input :cafe_id, :as => :select, :collection => Cafe.all.collect {|cafe| [cafe.title, cafe.id] }
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :image do |dish|
      image_tag dish.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
    end
    column :title do |dish|
      link_to dish.title, admin_dish_path(dish)
    end
    column :desc
    actions
  end

  show do
    attributes_table do
      row :image do |dish|
        image_tag dish.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
      end
      row :title
      row :desc
      row :cafe
    end
    admin_dishes_path
end
end
