# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
admin = User.create(name: 'Admin', email: 'admin@admin.com', password: '123456', is_admin: true)

def get_image_by_path(path)
    if File.exist?("#{path}.jpg")
        return File.open("#{path}.jpg")
    elsif File.exist?("#{path}.jpeg")
        return File.open("#{path}.jpeg")
    elsif File.exist?("#{path}.png")
        return File.open("#{path}.png")
    else 
        puts "file or directory not found (#{path})"
    end
end


def create_cafes
    cafes = []
    4.times do |i|
      fixtures_cafes_path = Rails.root.join('app', 'assets', 'images', 'fixtures_cafes', "#{i + 1}")
  
      cafe = Cafe.create(title: Faker::Restaurant.name, desc: Faker::Restaurant.description)

      file = get_image_by_path(fixtures_cafes_path)

      cafe.image.attach(io: file, filename: File.basename(file))

      cafes.push cafe
    end
    return cafes
  end

  def create_dishes(cafe_id, image_starting_index)
    4.times do |i|
      fixtures_dishes_path = Rails.root.join('app', 'assets', 'images', 'fixtures_dishes', "#{i + image_starting_index + 1}")
  
      dish = Dish.create(title: Faker::Restaurant.name, price: Faker::Number.between(from: 150, to: 700), desc: Faker::Restaurant.description, cafe_id: cafe_id)

      file = get_image_by_path(fixtures_dishes_path)

      dish.image.attach(io: file, filename: File.basename(file))
    end
  end

  cafes = create_cafes
  cafes.each_with_index do |cafe, i|
    create_dishes(cafe.id, i*4)
  end 

