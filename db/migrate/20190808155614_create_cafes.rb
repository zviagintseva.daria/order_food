class CreateCafes < ActiveRecord::Migration[5.2]
  def change
    create_table :cafes do |t|
      t.string :title
      t.string :image
      t.text :desc

      t.timestamps
    end
  end
end
