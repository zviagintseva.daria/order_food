class AddPlaceIdToDishes < ActiveRecord::Migration[5.2]
  def change
    add_reference :dishes, :cafe, foreign_key: true
  end
end
