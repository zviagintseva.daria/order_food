Rails.application.routes.draw do

  root 'cafes#index'
  get 'dishes' => 'dishes#index'
  get 'cafes/:id' => 'cafes#show', as: 'cafes_show'
  get 'dishes/:id' => 'dishes#show', as: 'dishes_show'
  
  ActiveAdmin.routes(self)
  devise_for :users
end
